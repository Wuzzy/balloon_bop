To create an arena, it is helpful to be familiar with Arena_lib's arena editor.

its not hard to figure out though.

replace stuff in <brackets> with appropriate variables

1) /arenas create balloon_bop <arena_name>
2) /arenas edit balloon_bop <arena_name>
3) you are given tools to help you create the arena. use the third tool in your inventory to set the sign of the arena. 
	Place a sign, then use the Plus tool to set it to the arena. Place the sign somewhere where players can easily access it to start the game.
	Go to where you want the arena to be. Then build the arena. 
	It should be inescapable, and have tall walls. For hardcore mode, it could be high in the air and have no walls.

	You can use the balloonblocks to make the arena.

4) Next, place the balloon popper spikes. They should be placed in such a way that you can bop the balloons into them to get points.
5) use the first tool in your arena edit inventory to set the number of players. Use the back tool.
6) use the second tool in your inventory to set the spawn locations (where the players will spawn when they enter the arena)
	Use the back tool.
7) use the gear tool (settings)
	*move to where you want the balloons to spawn, note the location (//1 from worldedit is helpful with that)
	*use the second tool in your inventory to open the arena properties editor
	*set the property "balloon_spawner" to be the location where you want balloons to spawn to float down to the players.
	*set the property "spawner_range" to be the distance from the spawner location that balloons can spawn.
	*set the property "arena_radius" to be large enough to encompass the entire arena from the balloon spawner.
	*set "player_die_level" to be the y-value below the floor of the arena (kills players who fall out of the arena and disqualifies balloons that fall that low)
	
	use the back tool and then enable the arena.

8) enjoy!

-> you can use /balloonbopscores <arena_name> to view the leaderboard anytime, if the arena has been played in.

