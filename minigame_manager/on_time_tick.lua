local rand = PcgRandom(os.time())



arena_lib.on_time_tick("balloon_bop", function(arena)
        if arena.in_game == false then 
                for _,obj in pairs(minetest.get_objects_inside_radius(arena.balloon_spawner, arena.arena_radius)) do
                        if obj and not( obj:is_player()) and obj:get_luaentity().name == "balloon_bop:balloon" then
                                obj:remove()
                        end
                end
                return
        end
        for pl_name,stats in pairs(arena.players) do
                arena.players[pl_name].score = arena.players[pl_name].score+1
        end
        arena.current_round_time = arena.current_round_time - 1
        if arena.current_round_time == 0 then
                arena.current_round_time = arena.round_time
                arena.num_balloons = arena.num_balloons + 1
                arena_lib.HUD_send_msg_all("broadcast", arena, arena.num_balloons .. " " .. balloon_bop.T("balloons!"), 1, nil, "0xB6D53C")
        end


        for pl_name,stats in pairs(arena.players) do -- it is a good convention to use "pl_name" in for loops and "p_name" elsewhere
                local player = minetest.get_player_by_name( pl_name )
                if player then
                        -- handle player "fall death"
                        local pos = player:get_pos()
                        if pos.y < arena.player_die_level then
                                player:set_pos(arena_lib.get_random_spawner(arena))
                        end
                        -- update score hud
                        if balloon_bop.infohuds[pl_name] then
                                player:hud_change(balloon_bop.infohuds[pl_name],"text",arena.players[pl_name].score)
                        end
                        -- set each players' nametag with their score
                        player:set_nametag_attributes({ 
                                text = tostring(arena.players[pl_name].score),
                                color = "#5A5353",
                                bgcolor = false,
                        })
                end
        end

        --handle balloons falling out or touching smth
        local bal_c = 0
        for _,obj in pairs(minetest.get_objects_inside_radius(arena.balloon_spawner, arena.arena_radius)) do
                if obj and not( obj:is_player()) and obj:get_luaentity().name == "balloon_bop:balloon" then

                        bal_c = bal_c + 1

                        if (obj:get_luaentity()._touching_ground == true or obj:get_pos().y <= arena.player_die_level) and obj:get_luaentity()._arena_name == arena.name then
                                minetest.sound_play("lose", {
                                        gain = 1.0,   -- default
                                        loop = false,
                                        pos = obj:get_pos()
                                    })
                                obj:remove()
                                -- deduct 20 points from everyone for letting a balloon touch!
                                for pl_name,stats in pairs(arena.players) do
                                        arena.players[pl_name].score = arena.players[pl_name].score-20
                                        if arena.players[pl_name].score < 0 then arena.players[pl_name].score = 0 end
                                end
                                arena.arena_lives = arena.arena_lives - 1
                                -- update the lives HUD
                                for pl_name,stats in pairs(arena.players) do
                                        local player = minetest.get_player_by_name( pl_name )
                                        if player and balloon_bop.numhuds[pl_name] ~= nil then
                                                for idx,hud_id in ipairs(balloon_bop.numhuds[pl_name]) do
                                                        if idx > arena.arena_lives then
                                                              player:hud_change(hud_id, "text", "blank.png")
                                                        end
                                                end
                                        end
                                end
                                --handle end of game
                                if arena.arena_lives == 0 then
                                        

                                        -- redo end-of-game
                                        local final_scores = {}
                                        
                                        for pl_name,stats in pairs(arena.players) do
                                                table.insert(final_scores,{name = pl_name,score=stats.score})
                                        end

                                        table.sort(final_scores, function (k1, k2) return k1.score > k2.score end )

                                        local winners = {}
                                        local win_data = {}

                                        local win_score = final_scores[1].score

                                        for i,data in ipairs(final_scores) do
                                                if data.score == win_score then
                                                        table.insert(winners,data.name)
                                                        table.insert(win_data,data)
                                                end
                                        end

                                        -- show leaderboards
                                        local l_data = {}
                                        for pl_name,stats in pairs(arena.players) do

                                                l_data[pl_name]=stats.score
                                                if balloon_bop.scores[arena.name][pl_name] then
                                                        if stats.score > balloon_bop.scores[arena.name][pl_name] then
                                                                balloon_bop.scores[arena.name][pl_name] = stats.score
                                                        end
                                                else
                                                        balloon_bop.scores[arena.name][pl_name] = stats.score
                                                end
                                        end
                                        balloon_bop.store_scores(balloon_bop.scores)
                                        for pl_name,stats in pairs(arena.players) do
                                                minetest.show_formspec(pl_name, "bb_scores_mp", balloon_bop.get_leader_form_endgame(arena.name,l_data))
                                        end
                                        if #winners == 1 then winners = winners[1] end
                                        arena_lib.load_celebration('balloon_bop', arena, winners)


                                        return
                                end
                        end
                end
        end
        if bal_c < arena.num_balloons and not(arena.in_celebration) then
                balloon_bop.spawn(arena)
        end
end)


arena_lib.on_quit("balloon_bop", function(arena, p_name, is_spectator)
        local player = minetest.get_player_by_name(p_name)
        if player then 
                if balloon_bop.infohuds[p_name] then
                        player:hud_remove(balloon_bop.infohuds[p_name])
                end
                if balloon_bop.numhuds[p_name] then
                        for idx,hud_id in ipairs(balloon_bop.numhuds[p_name]) do
                                player:hud_remove(hud_id)
                        end
                end
                balloon_bop.infohuds[p_name] = nil
                balloon_bop.numhuds[p_name] = nil
        end
end)
