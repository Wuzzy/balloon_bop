
local modname = "balloon_bop"
balloon_bop = {}
balloon_bop.T = minetest.get_translator("balloon_bop")



local round_time_default = 20


--====================================================
--====================================================
--               Minigame registration
--====================================================
--====================================================




arena_lib.register_minigame( modname , {
        name = "Balloon Bop",
        icon = "balloon_bop_hudballoon.png",

        properties = {
                starting_lives = 3,
                round_time = round_time_default,
                balloon_spawner = vector.new(0,7,0),
                spawner_range = 3.5,
                player_die_level = -1,
                arena_radius = 50,
        },

        player_properties = {
                score = 0,
        },
        temp_properties = {
                -- some minigames dont need temp properties
                num_balloons = 1,
                current_round_time = round_time_default,
                arena_lives = 1,
        },


                -- The prefix (string) is what's going to appear in most of the lines printed by your mod. Default is [Arena_lib]

        prefix = "["..modname.."] ",
        spectate = false,

        time_mode = 'incremental', -- for our sample minigame, we will use incrementing time. This will allow us to use on_time_tick if we want to.


        hotbar = {
                slots = 0,
                background_image = "blank.png", -- image not included!
                selected_image = "blank.png", -- image not included!
        },


        in_game_physics = {
                speed = 1.5,
                jump = 1,
                gravity = 1,
                sneak = true,},


        disabled_damage_types = {"fall","punch","set_hp"},
})



--====================================================
--====================================================
--            Calling the other files
--====================================================
--====================================================


local path = minetest.get_modpath(modname)


if not minetest.get_modpath("lib_chatcmdbuilder") then
        dofile(path .. "/libraries/chatcmdbuilder.lua")
end

local manager_path = path .. "/minigame_manager/"


dofile(path .. "/leaderboard.lua")
dofile(path .. "/nodes.lua")
dofile(path .. "/items.lua")
dofile(path .. "/blocks.lua")
dofile(manager_path .. "on_load.lua")
dofile(manager_path .. "on_start.lua")
dofile(manager_path .. "on_time_tick.lua")
dofile(manager_path .. "on_celebrate.lua")



