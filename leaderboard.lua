local storage = minetest.get_mod_storage()


function balloon_bop.store_scores(data)
    storage:set_string("scores", minetest.serialize(data))
end
function balloon_bop.get_scores()
    return minetest.deserialize(storage:get_string("scores"))
end

-- _bop.scores is a table with the forllowing structure:
-- {
--     [arena_name] ={
--         [p_name]=score,
--         [p_name]=score,
--         ..
--     },
--     [arena_name] ={
--         [p_name]=score,
--         [p_name]=score,
--         ..
--     },
--     ..
-- }

balloon_bop.scores = balloon_bop.get_scores() or {}





function balloon_bop.get_leader_form(arena_name,sel_idx)
    local p_names = "<no data>"
    sel_idx = sel_idx or 1

    if balloon_bop.scores[arena_name] then

        -- sort the scores
        local ordered_names = {}
        local data = balloon_bop.scores[arena_name]
        -- sort the scores
        for p_name,score in pairs(data) do
            table.insert(ordered_names,p_name)
        end
        table.sort(ordered_names,function(a,b)
            if balloon_bop.scores[arena_name][a] > balloon_bop.scores[arena_name][b] then return true else return false end
        end)

        if #ordered_names >=1 then
            p_names = ""
            for idx,u_name in ipairs(ordered_names) do
                local scorestr = tostring(data[u_name])
                local scorelen = string.len(scorestr)
                p_names = p_names .. scorestr
                for i = 1, 20-scorelen do
                    p_names = p_names .. " "
                end
                p_names = p_names .. u_name
                if idx ~= #ordered_names then
                    p_names = p_names .. ","
                end
            end
        end

    end

    return "formspec_version[5]"..
    "size[10.5,10.5]"..
    "background9[-.5,-.5;11.5,11.5;balloon_bop_leader_bg.png;false;125]"..
    "style_type[button,textlist;border=false;textcolor=#302C2E;font=normal,bold]"..
    "style[hs_title;font_size=+3]"..
    "button[0.6,0.6;9.3,0.8;hs_title;Balloon Bop Leaderboard]"..
    "button[0.6,1.6;9.3,0.8;arena_name;"..arena_name.."]"..
    "textlist[0.6,2.5;9.3,7.4;names;"..p_names..";"..sel_idx..";true]"

end



function balloon_bop.get_leader_form_endgame(arena_name,l_data,sel_idx,sel_idx2)
    sel_idx = sel_idx or 1
    sel_idx2 = sel_idx2 or 1
    local p_names = "<no data>"
    local lp_names = "<no data>"
    if balloon_bop.scores[arena_name] then

        local ordered_names = {}
        local data = balloon_bop.scores[arena_name]
        -- sort the scores
        for p_name,score in pairs(data) do
            table.insert(ordered_names,p_name)
        end
        table.sort(ordered_names,function(a,b)
            if balloon_bop.scores[arena_name][a] > balloon_bop.scores[arena_name][b] then return true else return false end
        end)


        if #ordered_names >=1 then
            p_names = ""
            for idx,u_name in ipairs(ordered_names) do
                local scorestr = tostring(data[u_name])
                local scorelen = string.len(scorestr)
                p_names = p_names .. scorestr
                for i = 1, 20-scorelen do
                    p_names = p_names .. " "
                end
                p_names = p_names .. u_name
                if idx ~= #ordered_names then
                    p_names = p_names .. ","
                end
            end
        end

    end


    if l_data then

        local ordered_names = {}
        local data = l_data
        -- sort the scores
        for p_name,score in pairs(data) do
            table.insert(ordered_names,p_name)
        end
        table.sort(ordered_names,function(a,b)
            if balloon_bop.scores[arena_name][a] > balloon_bop.scores[arena_name][b] then return true else return false end
        end)

        if #ordered_names >=1 then
            lp_names = ""
            for idx,u_name in ipairs(ordered_names) do
                local scorestr = tostring(data[u_name])
                local scorelen = string.len(scorestr)
                lp_names = lp_names .. scorestr
                for i = 1, 20-scorelen do
                    lp_names = lp_names .. " "
                end
                lp_names = lp_names .. u_name
                if idx ~= #ordered_names then
                    lp_names = lp_names .. ","
                end
            end
        end

    end

    return "formspec_version[5]"..
    "size[10.5,10.5]"..
    "background9[-.5,-.5;11.5,11.5;balloon_bop_leader_bg.png;false;125]"..
    "style_type[button,textlist;border=false;textcolor=#302C2E;font=normal,bold]"..
    "style[hs_title;font_size=+3]"..
    "button[0.6,0.6;9.3,0.8;hs_title;Balloon Bop Leaderboard]"..
    "button[0.6,1.6;9.3,0.8;arena_name;"..arena_name.."]"..
    "style_type[button,textlist;border=false;textcolor=#CFC6B8;font=normal,bold]"..
    "textlist[0.6,6.1;9.3,3.8;g_names;"..p_names..";"..sel_idx..";true]"..
    "textlist[0.6,3.3;9.3,2;l_names;"..lp_names..";"..sel_idx2..";true]"..
    "button[0.6,2.7;9.3,0.6;this;This Game]"..
    "button[0.6,5.5;9.3,0.6;high;LeaderBoard]"
end



minetest.register_chatcommand("balloonbopscores", {
    params = "<arena name>",  -- Short parameter description

    description = "Show leaderboard",  -- Full description

    func = function(name, param)
        if param then
            if balloon_bop.scores[param] then
                minetest.show_formspec(name, "bb_scores", balloon_bop.get_leader_form(param))
            else
                return false, balloon_bop.T("[!] No data for that arena or that arena does not exist!")
            end
        end
    end,
})
