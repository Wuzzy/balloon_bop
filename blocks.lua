-- Detect creative mod --
local creative_mod = minetest.get_modpath("creative")
-- Cache creative mode setting as fallback if creative mod not present --
local creative_mode_cache = minetest.settings:get_bool("creative_mode")

-- Returns a on_secondary_use function that places the balloon block in the air -- 
local placeColour = function (colour)
	return function(itemstack, user, pointed_thing)
		-- Place node three blocks from the user in the air --
		local pos = user:getpos()
		local dir = user:get_look_dir()
		local balloonPlaceDistanceFromPlayer = 3
		local new_pos = {
			x = pos.x + (dir.x * balloonPlaceDistanceFromPlayer),
			y = pos.y + 1 + (dir.y * balloonPlaceDistanceFromPlayer),
			z = pos.z + (dir.z * balloonPlaceDistanceFromPlayer),
		}
		local getPos = minetest.get_node(new_pos)
		if getPos.name == "air" or
				getPos.name == "default:water_source" or
				getPos.name == "default:water_flowing" or
				getPos.name == "default:river_water_source" or
				getPos.name == "default:river_water_flowing" then
			local name = 'balloon_bop:'..colour
			minetest.set_node(new_pos, {name=name})
			local creative_enabled = (creative_mod and creative.is_enabled_for(user.get_player_name(user))) or creative_mode_cache
			if (not creative_enabled) then
				local stack = ItemStack(name)
				return ItemStack(name .. " " .. itemstack:get_count() - 1)
			end
		end
	end
end

local soundsConfig = function ()
	return {
	  footstep = {name = "balloon_bop_footstep", gain = 0.2},
	  dig = {name = "balloon_bop_footstep", gain = 0.3},
	  dug = {name = "default_dug_hard.1", gain = 0.3},
	  place = {name = "default_place_node_hard", gain = 1.0}
	}
end

-- Holds balloonblock functions and config --
local state = {
	placeRed = placeColour('red'),
	placeYellow = placeColour('yellow'),
	placeGreen = placeColour('green'),
	placeBlue = placeColour('blue'),
	placeBlack = placeColour('black'),
	placeWhite = placeColour('white'),
	placeOrange = placeColour('orange'),
	placePurple = placeColour('purple'),
	placeGrey = placeColour('grey'),
	placePink = placeColour('pink'),
	placeBrown = placeColour('brown'),
	sounds = soundsConfig(),
	groups = {snappy=3, fall_damage_add_percent = -99, bouncy=70}
}
-- Normal balloon_bop --

minetest.register_node("balloon_bop:red", {
  description = "Red balloon",
  tiles = {"balloon_bop_red.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeRed,
	sounds = state.sounds
})


minetest.register_node("balloon_bop:yellow", {
	description = "Yellow balloon",
	tiles = {"balloon_bop_yellow.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeYellow,
	sounds = state.sounds
})


minetest.register_node("balloon_bop:green", {
  description = "Green balloon",
  tiles = {"balloon_bop_green.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeGreen,
	sounds = state.sounds
})

minetest.register_node("balloon_bop:blue", {
  description = "Blue balloon",
  tiles = {"balloon_bop_blue.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeBlue,
	sounds = state.sounds
})



minetest.register_node("balloon_bop:black", {
  description = "Black balloon",
  tiles = {"balloon_bop_black.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeBlack,
	sounds = state.sounds
})



minetest.register_node("balloon_bop:white", {
  description = "White balloon",
  tiles = {"balloon_bop_white.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeWhite,
	sounds = state.sounds
})



minetest.register_node("balloon_bop:orange", {
  description = "Orange balloon",
  tiles = {"balloon_bop_orange.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeOrange,
	sounds = state.sounds
})


minetest.register_node("balloon_bop:purple", {
  description = "Purple balloon",
  tiles = {"balloon_bop_purple.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placePurple,
	sounds = state.sounds
})


minetest.register_node("balloon_bop:grey", {
  description = "Grey balloon",
  tiles = {"balloon_bop_grey.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeGrey,
	sounds = state.sounds
})


minetest.register_node("balloon_bop:pink", {
  description = "Pink balloon",
  tiles = {"balloon_bop_pink.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placePink,
	sounds = state.sounds
})



minetest.register_node("balloon_bop:brown", {
  description = "Brown balloon",
  tiles = {"balloon_bop_brown.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	on_secondary_use = state.placeBrown,
	sounds = state.sounds
})
