
-- This code is run when the game has finished the loading phase and starts the game timer. 

-- For the sample minigame, we will use it to give each player a sword, and tell them to fight


-- replace "sample_minigame" with the mod name here
balloon_bop.infohuds = {}
balloon_bop.numhuds = {}

arena_lib.on_start("balloon_bop", function(arena)


        arena.num_balloons = #arena.players
        if arena.num_balloons == 0 then arena.num_balloons = 1 end
        arena.arena_lives = arena.starting_lives
        arena.current_round_time = arena.round_time
        for pl_name,stats in pairs(arena.players) do
                local player = minetest.get_player_by_name( pl_name )
                if player then
                        balloon_bop.infohuds[pl_name] = player:hud_add({
                                hud_elem_type   = "text",
                                number          = 0xE6482E,
                                position        = { x = .97, y = .03},
                                offset          = {x = 0,   y = 0},
                                text            = arena.players[pl_name].score,
                                alignment       = {x = -1, y = 1},
                                scale           = {x = 100, y = 100},
                                size            = {x = 2 },
                        })
                        local hud_list = {}
                        for i=1,arena.arena_lives do

                                balloon_bop.numhuds[pl_name] = {}
                                table.insert(hud_list, player:hud_add({
                                        hud_elem_type   = "image",
                                        number          = 0xE6482E,
                                        position        = { x = .97, y = .1},
                                        offset          = {x = -3*18*(i-1),   y = 0},
                                        text            = "balloon_bop_hudballoon.png",
                                        alignment       = {x = -1, y = 1},
                                        scale           = {x = 3, y = 3},
                                }))

                        end
                        balloon_bop.numhuds[pl_name] = hud_list
                end
        end
        arena_lib.HUD_send_msg_all("broadcast", arena, "1" .. " " .. balloon_bop.T("balloon!"), 1, nil, "0xB6D53C")

        balloon_bop.spawn(arena)


end)


arena_lib.on_join("balloon_bop", function(p_name, arena, as_spectator)
        if not(as_spectator) then 
                local player = minetest.get_player_by_name( p_name )
                if player then
                        balloon_bop.infohuds[p_name] = player:hud_add({
                                hud_elem_type   = "text",
                                number          = 0xE6482E,
                                position        = { x = .97, y = .03},
                                offset          = {x = 0,   y = 0},
                                text            = arena.players[p_name].score,
                                alignment       = {x = -1, y = 1},
                                scale           = {x = 100, y = 100},
                                size            = {x = 2 },
                        })
                        local hud_list = {}
                        for i=1,arena.arena_lives do

                                balloon_bop.numhuds[p_name] = {}
                                table.insert(hud_list, player:hud_add({
                                        hud_elem_type   = "image",
                                        number          = 0xE6482E,
                                        position        = { x = .97, y = .1},
                                        offset          = {x = -3*18*(i-1),   y = 0},
                                        text            = "balloon_bop_hudballoon.png",
                                        alignment       = {x = -1, y = 1},
                                        scale           = {x = 3, y = 3},
                                }))

                        end
                        balloon_bop.numhuds[p_name] = hud_list
                end
        end
end)