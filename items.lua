-- licensed as follows:
-- MIT License, ExeVirus (c) 2021, MisterE (c) 2022
--
-- Please see the LICENSE file for more details

local spawn_time = 10
local time = 0


local rand = PcgRandom(os.time())

local function add_points(self,pts)
    -- minetest.chat_send_all(self._arenaid)
    if self._arenaid then
        if arena_lib.mods["balloon_bop"].arenas[self._arenaid].in_game == true then
            local arena = arena_lib.mods["balloon_bop"].arenas[self._arenaid]
            if self._player then
                local score = arena.players[self._player].score
                arena.players[self._player].score = score + tonumber(self._points)
                -- show points particle
                local pos = self.object:get_pos()
                local o_texture = self._original_texture
                minetest.add_particlespawner({
                    amount = 1,
                    time = .5,
                    minpos=vector.new(pos.x,pos.y+4,pos.z),
                    maxpos=vector.new(pos.x,pos.y+3.5,pos.z),
                    minvel=vector.new(0,1,0),
                    maxvel=vector.new(0,1,0),
                    minacc = vector.new(0,0,0),
                    maxacc = vector.new(0,0,0),
                    minexptime = 2,
                    maxexptime = 2,
                    minsize = 10,
                    maxsize = 13,
                    collisiondetection = false,
                    vertical = false,
                    texture="balloon_bop_point_screen_"..self._points..".png^[mask:"..o_texture .. "^[opacity:170",
                    playername = self._player,
                })
            end
        end
    end
end

-- Balloon


balloon_bop.spawn = function(arena)
    local obj = minetest.add_entity(vector.new(
            arena.balloon_spawner.x+rand:next(1,7)-arena.spawner_range,
            arena.balloon_spawner.y,
            arena.balloon_spawner.z+rand:next(1,7)-arena.spawner_range)
    , "balloon_bop:balloon", minetest.write_json({
        _arenaid = arena_lib.get_arena_by_name("balloon_bop", arena.name),
        _arena_name = arena.name
    }))
    minetest.sound_play("balloon", {
            gain = 1.0,   -- default
            loop = false,
            pos = arena.balloon_spawner,
    })
end


local balloon = {
    initial_properties = {
        hp_max = 10,
        visual = "mesh",
        visual_size = {x=0.1,y=0.117,z=0.1},
        -- glow = 10,
        static_save = false,
        mesh = "balloon.obj",
        physical = true,
        collide_with_objects = true,
        collisionbox = {-0.6, -0.5, -0.6, 0.6, 0.5, 0.6},
        textures = {"balloon_1.png"},
    },

    --Physics, and collisions
    on_step = function(self, dtime, moveresult)
        if self._arenaid then
            if arena_lib.mods["balloon_bop"].arenas[self._arenaid].in_game == false then
                self.object:remove()
                return
            end
        end

        if minetest.find_node_near(self.object:get_pos(), 1.5, {"balloon_bop:spike"}) then
            -- pop balloon harmlessly 
            minetest.sound_play("balloon_pop", {
                gain = 1.0,   -- default
                loop = false,
                pos = self.object:get_pos()
            })
            -- balloon particles
            local pos = self.object:get_pos()
            local o_texture = self._original_texture
            local vel = 10
            minetest.add_particlespawner({
                amount = 35,
                time = .1,
                minpos=vector.new(pos.x-1.1,pos.y-1.1,pos.z-1.1),
                maxpos=vector.new(pos.x+1.1,pos.y+1.1,pos.z+1.1),
                minvel=vector.new(-vel,-vel,-vel),
                maxvel=vector.new(vel,vel,vel),
                minacc = vector.new(0,0,0),
                maxacc = vector.new(0,0,0),
                minexptime = .05,
                maxexptime = .05,
                minsize = 10,
                maxsize = 13,
                collisiondetection = false,
                vertical = false,
                texture="balloon_bop_scrap.png".."^[mask:"..o_texture .. "^[opacity:170",
            })
            -- add points
            add_points(self,25)
            --remove balloon
            self.object:remove()
            return
        end

        if moveresult.touching_ground and not(moveresult.standing_on_object) then
            self._touching_ground = true
        else
            --slow our x,z velocities
            local vel = self.object:get_velocity()
            if vel == nil then vel = {x=0,y=0,z=0} end
            self.object:set_velocity({x=vel.x*0.97, y=vel.y*0.97, z=vel.z*0.97})
            local rot = self.object:get_rotation()
            self.object:set_rotation(vector.new(rot.x,rot.y+.04,rot.z))
        end
    end,

    --Punch Physics
    on_punch = function(self, puncher, _, _, dir)
        self.object:set_velocity(dir*25)
        minetest.sound_play("punch", {
            gain = 1.0,   -- default
            loop = false,
            pos = self.object:get_pos()
        })
        if not (puncher:is_player()) then return end
        self._player = puncher:get_player_name()
    end,

    --Setup fallspeed
    on_activate = function(self, staticdata, dtime_s)
        math.randomseed = os.time()
        local point_chance = math.random(1,3)
        if point_chance == 1 then
            self._points = "10"
        elseif point_chance == 2 then
            self._points = "15"
        else
            self._points = "20"
        end


        self.object:set_acceleration({x=0,y=-3.0,z=0})
        local props = self.object:get_properties()
        local text = "balloon_" .. rand:next(1,4) .. ".png"
        self._original_texture = text
        props.textures = {
            text.."^(balloon_bop_point_screen_"..self._points..".png^[colorize:#302C2E:255)",
        }
        props.automatic_rotate = 40,
        self.object:set_properties(props)
        minetest.sound_play("balloon_inflate", {
            gain = 1.0,   -- default
            loop = false,
            pos = self.object:get_pos()
        })
        if staticdata ~= "" and staticdata ~= nil then
            local data = minetest.parse_json(staticdata) or {}
            if data._arenaid then
                self._arenaid = data._arenaid
            end
            if data._arena_name then
                self._arena_name = data._arena_name
            end
        end
    end,
    _touching_ground = false,
    _player = nil,
    _points = nil,

}

minetest.register_entity("balloon_bop:balloon", balloon)
